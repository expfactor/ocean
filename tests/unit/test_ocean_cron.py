# flake8: noqa
import os

import mock
import ocean
import ocean.modules.mixin_crontab


def test_ocean_cron_register():
    """Ocean has a cron register command."""
    manifest = "environments.yaml"
    with mock.patch('os.environ') as mock_os_environ:
        with mock.patch('ocean.modules.mixin_crontab.open') as mock_open:
            with mock.patch('ocean.modules.mixin_crontab.os') as mock_os:
                mock_os_environ.get = mock.MagicMock(side_effect=lambda x, y=None: y)
                ocean.main(["cron_register"],
                           force=True,
                           MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
                           )


def test_ocean_cron_exec():
    """Ocean has a cron register command."""
    manifest = "cron"
    ocean.main(["cron_run_hourly"],
               force=True,
               MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
               )
    ocean.main(["cron_run_daily"],
               force=True,
               MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
               )
    ocean.main(["cron_run_weekly"],
               force=True,
               MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
               )
    ocean.main(["cron_run_monthly"],
               force=True,
               MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
               )
