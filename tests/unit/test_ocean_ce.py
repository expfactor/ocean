# flake8: noqa
import functools
import os

import mock
from ocean import get_ocean


def env_test(nt):
    def new_test():
        manifest = "environments.yaml"
        with mock.patch('os.environ') as mock_ocean_os_environment:
            with mock.patch('ocean.modules.mixin_compute_environment.subprocess') as mock_subprocess:
                mock_ocean_os_environment.get = mock.MagicMock(side_effect=lambda x, y=None: y)
                comres = mock.MagicMock()
                comres.returncode = 0
                comres.communicate.return_value = None, None
                mock_subprocess.Popen.return_value = comres
                ocean = get_ocean(True,
                                  MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest)
                                  )
                nt(ocean)
    return functools.wraps(nt)(new_test)


# ensures that the command are not faulty
@env_test
def test_ce_push(*args):
    ocean = args[0]
    ocean.ce_push('test-env')


@env_test
def test_ce_freeze(*args):
    ocean = args[0]
    ocean.ce_freeze('test-env')


@env_test
def test_ce_start(*args):
    ocean = args[0]
    ocean.ce_start('test-env')


@env_test
def test_ce_kill(*args):
    ocean = args[0]
    ocean.ce_kill('test-env')


@env_test
def test_ce_status(*args):
    ocean = args[0]
    ocean.ce_status('test-env')


@env_test
def test_ce_build(*args):
    ocean = args[0]
    ocean.ce_build('test-env')
