## Deploying web-services with ocean

OCEAN manages for you numerous aspects of the lifecycle related to webservices.
Including:

- NGINX as proxy
- Load balancing
- DNS record management
- Proxy
- SSL security and certificate renewal
- Failures or services and restart

To add a publish a service on the web, simply add a `nginx` section to its manifest.

The set services accessible via the `proxy` `nginx` will be displayed by

`ocean requirements nginx`

Typically this will display a tree with all the required services:

```
nginx
    dev-env-dfract-http
        dev-env-dfract-hub
            dev-env-dfract-celery-worker
                dev-env-dfract-redis-celery-backend
            dev-env-dfract-mongo
            dev-env-dfract-pyro-ns
    dev-env-sensingfeeling-grafana
        lab-env-ldap
    dev-env-sensingfeeling-influxdb
    dev-env-sensingfeeling-sfta
    lab-env-docker-registry
        lab-env-docker-registry-redis
    lab-env-docker-registry-frontend
        lab-env-docker-registry
            lab-env-docker-registry-redis
    lab-env-efk0
    lab-env-gitlab
        lab-env-gitlab-postgres
        lab-env-gitlab-redis
    lab-env-influxdb
    lab-env-ldap-admin
        lab-env-ldap
    lab-env-ldap-config
    lab-env-password
    prod-env-dfract-http
        prod-env-dfract-hub
            prod-env-dfract-celery-worker
                prod-env-dfract-redis-celery-backend
            prod-env-dfract-mongo
            prod-env-dfract-pyro-ns
    prod-env-sensingfeeling-grafana
        lab-env-ldap
    prod-env-sensingfeeling-influxdb
    uat-env-dfract-http
        uat-env-dfract-hub
            uat-env-dfract-celery-worker
                uat-env-dfract-redis-celery-backend
            uat-env-dfract-mongo
            uat-env-dfract-pyro-ns
    uat-env-sensingfeeling-grafana
        lab-env-ldap
    uat-env-sensingfeeling-influxdb
    website-algo-community
        website-algo-community-postgres0
        website-algo-community-redis0

```


## Updating DNS records:
`ocean register_cnames


## Preparing for SSL

By default, ocean will create self-signed certificate for all services ran via ssl. On production
sites it is possible to use ocean to request for certificates for letsencrypt.
This is done simply by running:

The first time:

`ocean nginx_lets_encrypt_init`

And, in subsequent calls:

`ocean nginx_lets_encrypt_update`

